package com.manixate.autosmsresponder;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class SMSReceiver extends BroadcastReceiver {
    static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";

    public SMSReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (SMS_RECEIVED.equals(action)) {
            Bundle myBundle = intent.getExtras();

            if (myBundle != null)
            {
                String numbersList = context.getSharedPreferences(MainActivity.PREFS_NAME, Context.MODE_PRIVATE).getString(MainActivity.KEY, "[]");
                JSONArray jsonArray = null;
                ArrayList<String> numbersArrayList = new ArrayList<String>();
                try {
                    jsonArray = new JSONArray(numbersList);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        numbersArrayList.add(jsonArray.getString(i));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Object [] pdus = (Object[]) myBundle.get("pdus");

                for (Object pdu : pdus) {
                    SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdu);
                    String sender = smsMessage.getOriginatingAddress();
//                    String messageBody = smsMessage.getMessageBody();

                    boolean contains = false;
                    for (String number : numbersArrayList) {
                        if (sender.endsWith(number)) {
                            contains = true;
                            break;
                        }
                    }

                    if (!contains) {
                        continue;
                    }

                    String replyText = "Got your message";

//                    Intent sentIntent = new Intent(SMS_SENT);
//                    sentIntent.putExtra("number", sender);
//                    sentIntent.putExtra("message", replyText);
//
//                    PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, sentIntent, PendingIntent.FLAG_ONE_SHOT);

                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(sender, smsMessage.getServiceCenterAddress(), replyText, null, null);

                    Log.d("SMS_SENDING_APP", "Reply to " + sender + ":\n " + replyText);

                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.ic_launcher)
                                    .setContentTitle("SMSReply App Notification")
                                    .setContentText("Received message from " + sender);
                    NotificationManager mNotifyMgr =
                            (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    mNotifyMgr.notify(1200, mBuilder.build());

                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    Ringtone ringtone = RingtoneManager.getRingtone(context.getApplicationContext(), notification);
                    ringtone.play();

                    abortBroadcast();
                }
            }
        }
    }
}

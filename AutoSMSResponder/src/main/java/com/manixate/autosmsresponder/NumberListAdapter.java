package com.manixate.autosmsresponder;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Muhammad on 12/23/13.
 */
public class NumberListAdapter extends BaseAdapter {
    ArrayList<String> mNumbersList;
    final Context mContext;

    public NumberListAdapter(Context context, ArrayList<String> numbersList) {
        this.mContext = context;
        this.mNumbersList = numbersList;
    }

    public void addItem(String item) {
        mNumbersList.add(item);

        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        mNumbersList.remove(position);

        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mNumbersList.size();
    }

    @Override
    public Object getItem(int i) {
        return mNumbersList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        TextView phoneTextView;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext)
                    .inflate(android.R.layout.simple_list_item_1, null, false);
            phoneTextView = (TextView) convertView.findViewById(android.R.id.text1);
            convertView.setTag(new ViewHolder(phoneTextView));
        } else {
            ViewHolder viewHolder = (ViewHolder) convertView.getTag();
            phoneTextView = viewHolder.phoneTextView;
        }

        String number = (String) getItem(i);
        phoneTextView.setText(number);

        return convertView;
    }

    private static class ViewHolder {
        public final TextView phoneTextView;

        public ViewHolder(TextView phoneTextView) {
            this.phoneTextView = phoneTextView;
        }
    }
}

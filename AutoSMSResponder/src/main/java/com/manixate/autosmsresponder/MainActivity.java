package com.manixate.autosmsresponder;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class MainActivity extends Activity {
    public static final String PREFS_NAME = "SMSResponder";
    public static final String KEY = "Numbers";

    NumberListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String numbersList = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).getString(KEY, "[]");
        JSONArray jsonArray = null;
        ArrayList<String> numbersArrayList = new ArrayList<String>();
        try {
            jsonArray = new JSONArray(numbersList);
            for (int i = 0; i < jsonArray.length(); i++) {
                numbersArrayList.add(jsonArray.getString(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setEmptyView(findViewById(R.id.emptyElement));

        mAdapter = new NumberListAdapter(this, numbersArrayList);

        listView.setAdapter(mAdapter);

        SwipeDismissListViewTouchListener touchListener =
                new SwipeDismissListViewTouchListener(
                        listView,
                        new SwipeDismissListViewTouchListener.DismissCallbacks() {
                            @Override
                            public boolean canDismiss(int position) {
                                return true;
                            }

                            @Override
                            public void onDismiss(ListView listView, int[] reverseSortedPositions) {
                                for (int position : reverseSortedPositions) {
                                    mAdapter.removeItem(position);
                                }
                            }
                        });

        listView.setOnTouchListener(touchListener);
        listView.setOnScrollListener(touchListener.makeScrollListener());

        Button addBtn = (Button) findViewById(R.id.addBtn);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.FROYO)
    private void showDialog() {

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_PHONE);

        final AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setView(input)
                .setTitle("Please enter number")
                .setPositiveButton(android.R.string.ok, null) //Set to null. We override the onclick
                .setNegativeButton(android.R.string.cancel, null)
                .create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {

                Button positiveBtn = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                if (positiveBtn != null) {
                    positiveBtn.setOnClickListener(new View.OnClickListener() {
                     @Override
                        public void onClick(View view) {
                            String value = input.getText().toString();

                            if (value.length() >= 10) {
                                mAdapter.addItem(value.substring(value.length() - 10));
                                alertDialog.dismiss();
                            } else {
                                Toast.makeText(MainActivity.this, "Please enter valid number", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }

                Button negativeBtn = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                if (negativeBtn != null) {
                    negativeBtn.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();
                        }
                    });
                }
            }
        });

        alertDialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();

        ArrayList<String> items = mAdapter.mNumbersList;
        JSONArray jsonArray = new JSONArray(items);

        getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit().putString(KEY, jsonArray.toString()).commit();
    }
}
